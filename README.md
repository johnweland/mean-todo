# MEAN-TODO
This is a basic 'TODO' app using MongoDB, Express, Angular and Node.
Built using a code along course from [Treehouse](https://teamtreehouse.com/library/building-a-mean-application).

You will need

* nodejs [version 5.7.1](https://nodejs.org/en/) 
* MongoDB [version 3.2.3](https://www.mongodb.org/downloads#production)
* Express
* Angular (use this one or create a new one)


# set-up
```
1. Fork this repo
1. `git clone <your-fork>`
1. Follow along with course and hack away
1. `git commit -am "commit some cool changes"`
1. `git push origin someCoolBranch` pushes changes to your fork. Great for sharing code and asking for comments on commits.